package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

// Tabs gotten from: http://www.gadgetsaint.com/android/create-viewpager-tabs-android/#.WpwMa0xFx0w
public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    //private ListAdapter adapter;
    // Write a message to the database
    public static FirebaseDatabase database = FirebaseDatabase.getInstance();      // Public so it can be used in other classes.
    public static DatabaseReference messageRef = database.getReference("messages");
    public static DatabaseReference userRef = database.getReference("users");
    private String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Obtain the FirebaseAnalytics instance.
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        // Add Fragments to adapter one by one
        adapter.addFragment(new FragmentOne(), "All messages");
        adapter.addFragment(new FragmentTwo(), "Friends");
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        mAuth = FirebaseAuth.getInstance();

        // active listen to user logged in or not.
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    logged_in();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }

            }
        };

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String username = prefs.getString("username","null");

        // If user has logged in successfully before, and username is saved.
        if (!Objects.equals(username, "null")) {
            logged_in();
        }
        else {                      // If not logged in successfully before
            logIn();
        }


        // Read from the database, order by date added
        messageRef.orderByChild("messageTime").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                displayChatMessages(dataSnapshot);
                //Log.d(TAG, "onDataChange: Data: " + dataSnapshot.getValue(String.class));
               // Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        // Read from the database, order alfabeticlly by username.
        userRef.orderByChild("username").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                displayFriends(dataSnapshot);
                //Log.d(TAG, "onDataChange: Data: " + dataSnapshot.getValue(String.class));
                // Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        GetChatMessageService.startActionCheckNew(this,"","");
    }

    // Add Auth state listener in onStart method.
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    // release listener in onStop
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: Result OK");
                String result = data.getStringExtra("username");

                // Check if username is not in database already.
                //Get datasnapshot at your "users" root node
                /*userRef.addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                //Get map of users in datasnapshot
                                Map<String,Object> users = (Map<String,Object>) dataSnapshot.getValue();
                                ArrayList<User> userArray = new ArrayList<>();

                                //iterate through each user, ignoring their UID
                                for (Map.Entry<String, Object> entry : users.entrySet()){

                                    // Get user in User-class
                                    User singleUser = (User) entry.getValue();
                                    // Append user to list
                                    userArray.add(singleUser);
                                }

                                boolean isUnique = true;        //Suppose it is not in database
                                for( User user : userArray ) {
                                    if (newUser == user.getUsername()) {        // If in database, set to false and break.
                                        isUnique = false;
                                        break;
                                    }
                                }
                                if (isUnique) {
                                    signInAnonymously(newUser);
                                }
                                else {
                                    Toast.makeText(MainActivity.this, "Username already exist in database, find a new one.", Toast.LENGTH_SHORT).show();
                                    logIn();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                //handle databaseError
                            }
                        });*/
                signInAnonymously(result);

            }

            if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: Result not OK");
                // No result found, ask again
                logIn();
            }
        }
    }

    void logIn() {
        Intent startSecondActivityIntent = new Intent(this, LogIn.class);
        startActivityForResult(startSecondActivityIntent, 1);
    }

    void logged_in() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String username = prefs.getString("username","null");
        Toast.makeText(MainActivity.this, "Welcome " + username, Toast.LENGTH_SHORT).show();
        // Load chat room contents
        //displayChatMessages();
    }

    void signInAnonymously(final String username) {
        //Log in.
        mAuth.signInAnonymously().addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "OnComplete : " +task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Failed : ", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Log.w(TAG, "Success for signing in.");
                            successLogin(username);
                        }
                    }
                });
    }

    void successLogin(String username) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString("username",username);
        editor.apply();
        Log.d(TAG, "successLogin: Writing user to database");
        // Write a chat mssage to the database
        userRef.push().setValue(new User(username));
        Log.d(TAG, "successLogin: Wrote user to database");
    }

    public void displayChatMessages(DataSnapshot dataSnapshot) {
        new GetData(this, dataSnapshot).execute();
        //showNotification("New message", "New message is submitted");
    }

    public void displayFriends(DataSnapshot dataSnapshot) {
        new GetFriends(this, dataSnapshot).execute();
    }

    public void addMessage(View v) {
        EditText input = (EditText)findViewById(R.id.et_text);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String username = prefs.getString("username","null");

        if (username != "null") {
            Log.d(TAG, "addMessage: Writing message to database");
            // Write a chat mssage to the database
            messageRef.push().setValue(new ChatMessage(input.getText().toString(),
                            username)
                    );
            Log.d(TAG, "addMessage: Wrote message to database");
        }
        else {
            Log.d(TAG, "addMessage: An error occured, did not get correct id.");
        }


        // Clear the input
        input.setText("");
    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}