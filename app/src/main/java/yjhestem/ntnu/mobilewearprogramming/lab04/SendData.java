package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Bruker on 06.03.2018.
 */

class SendData extends AsyncTask<String, Void, String> {
    Context context;
    //public Activity activity;
    private String username;
    private String message;

    private String TAG = "SendData";
//.... other attributes

    public SendData(Context _context, String _username, String _message){

        this.context = _context.getApplicationContext();
        this.username = _username;
        this.message = _message;
//other initializations...

    }


    @Override
    protected String doInBackground(String... params) {
        Log.d(TAG, "doInBackground: Starting..");
        String result;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");
        myRef.push().setValue(new ChatMessage(message,
                username)
        );
        result = "ok";
        return result;

    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("lab2", "onPostExecute: " + result);
        Toast.makeText(context,"Saved message",Toast.LENGTH_SHORT);

        super.onPostExecute(result);
    }
}
