package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.firebase.database.DataSnapshot;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by Bruker on 08.02.2018.
 *
 * This use the gotted message-data and add the proper messages to listview.
 */

class GetData extends AsyncTask<String, Void, SimpleAdapter> {
    private static final String TAG = "GetData";
    //Context context;
    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    private DataSnapshot dataSnapshot;
    private String user;                 //If just to get one user
//.... other attributes

    GetData(Activity _activity, DataSnapshot _dataSnapshot){

        this.activity = _activity;
        this.dataSnapshot = _dataSnapshot;
        this.user = "";
//other initializations...

    }

    GetData(Activity _activity, DataSnapshot _dataSnapshot, String _user){

        this.activity = _activity;
        this.dataSnapshot = _dataSnapshot;
        this.user = _user;
//other initializations...

    }


    @Override
    protected SimpleAdapter doInBackground(String... params) {
        Log.d("lab2", "doInBackground: Starting..");
        Log.d(TAG, "displayChatMessages: Starting..");
        List<HashMap<String, String>> messages = new ArrayList<HashMap<String,String>>();
        Log.d(TAG, "displayChatMessages: Starting to go through messages");
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            Log.d(TAG, "displayChatMessages: Starting to go through new message");
            ChatMessage message = postSnapshot.getValue(ChatMessage.class);
            Boolean shallShow = true;               // If this message shall be shown.
            Log.d(TAG, "doInBackground: User to show is (if nothing here, accept all): " + user);
            assert message != null;
            if (!Objects.equals(user, "") && !Objects.equals(user, message.getMessageUser())) {
                Log.d(TAG, "doInBackground: Shall not show this message, user who wrote this message is " + message.getMessageUser());
                shallShow = false;
            }
            if (shallShow) {
                Log.d(TAG, "doInBackground: Shall show this message");
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("username", message.getMessageUser());
                hm.put("text", message.getMessageText());
                hm.put("timestamp", message.getMessageTime());
                messages.add(hm);
            }
            Log.d(TAG, "displayChatMessages: Finished with this message");
        }
        Log.d(TAG, "displayChatMessages: Add messages to listview");

        // Keys used in Hashmap
        String[] from = { "username","text","timestamp"};

        // Ids of views in listview_layout
        int[] to = { R.id.message_user,R.id.message_text,R.id.message_time};

        Log.d(TAG, "displayChatMessages: Start new adapter");

        return new SimpleAdapter(activity.getBaseContext(), messages, R.layout.message, from, to);
    }

    @Override
    protected void onPostExecute(SimpleAdapter adapter) {
        ListView listOfMessages = (ListView)activity.findViewById(R.id.lv_messages);
        listOfMessages.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added messages to listview.");
    }
}