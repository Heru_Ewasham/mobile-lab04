package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bruker on 08.03.2018.
 *
 * This is a class to store user information.
 */

public class User {
    private String username;
    private String joinTime;

    User(String username) {
        this.username = username;

        // Initialize to current time
        Date date=new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.joinTime = sfd.format(date);
    }

    public User(){

    }

    String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String time) {
        this.joinTime = time;
    }
}
