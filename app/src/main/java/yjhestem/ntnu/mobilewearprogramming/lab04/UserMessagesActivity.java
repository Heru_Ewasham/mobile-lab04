package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class UserMessagesActivity extends AppCompatActivity {
    private String user;
    private String TAG = "UserMessageActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_messages);

        Intent intent = getIntent();
        user = intent.getStringExtra("user");

        // Read from the database
        MainActivity.messageRef.orderByChild("messageTime").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                displayChatMessagesForUser(dataSnapshot);
                //Log.d(TAG, "onDataChange: Data: " + dataSnapshot.getValue(String.class));
                // Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void displayChatMessagesForUser(DataSnapshot dataSnapshot) {
        new GetData(this, dataSnapshot, user).execute();
        Log.d(TAG, "displayChatMessagesForUser: Showing user Result for user " + user);
    }
}
