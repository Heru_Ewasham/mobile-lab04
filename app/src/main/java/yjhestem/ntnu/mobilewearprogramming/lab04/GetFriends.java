package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Bruker on 09.03.2018.
 *
 * This converts the gotted friends-list and add it to our listview.
 */

class GetFriends extends AsyncTask<String, Void, SimpleAdapter> {
    private static final String TAG = "GetFriends";
    //Context context;
    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    private DataSnapshot dataSnapshot;
//.... other attributes

    GetFriends(Activity _activity, DataSnapshot _dataSnapshot){

        this.activity = _activity;
        this.dataSnapshot = _dataSnapshot;
//other initializations...

    }


    @Override
    protected SimpleAdapter doInBackground(String... params) {
        Log.d(TAG, "doInBackground: Starting..");
        List<HashMap<String, String>> users = new ArrayList<HashMap<String,String>>();
        Log.d(TAG, "displayChatMessages: Starting to go through users");
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            Log.d(TAG, "displayChatMessages: Starting to go through new user");
            User user = postSnapshot.getValue(User.class);
            HashMap<String, String> hm = new HashMap<String, String>();
            assert user != null;
            hm.put("username", user.getUsername());
            hm.put("timestamp", "Join date" + user.getJoinTime());
            users.add(hm);
            Log.d(TAG, "doInBackground: Finished with this user");
        }
        Log.d(TAG, "doInBackground: Add user to listview");

        // Keys used in Hashmap
        String[] from = { "username","timestamp"};

        // Ids of views in listview_layout
        int[] to = { R.id.user_user,R.id.user_joinTime};

        Log.d(TAG, "doInBackground: Start new adapter");

        return new SimpleAdapter(activity.getBaseContext(), users, R.layout.user, from, to);
    }

    @Override
    protected void onPostExecute(SimpleAdapter adapter) {
        ListView listOfFriends = (ListView)activity.findViewById(R.id.lv_friends);
        listOfFriends.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added messages to listview.");
    }
}
