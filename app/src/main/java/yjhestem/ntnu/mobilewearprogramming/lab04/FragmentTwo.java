package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.content.Intent;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Bruker on 04.03.2018.
 *
 * Fragment Two is the Friends-list.
 */

public class FragmentTwo extends Fragment {
    private static final String TAG = "FragmentTwo";

    public FragmentTwo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        ListView lvFriends = (ListView)view.findViewById(R.id.lv_friends);
        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.user_user);
                String user = textView.getText().toString();
                Intent intent = new Intent(view.getContext(), UserMessagesActivity.class);
                Log.d(TAG, "On item click: User to send: " + user);
                intent.putExtra("user", user);
                startActivity(intent);
            }
        });
        return view;
    }


}
