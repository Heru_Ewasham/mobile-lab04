package yjhestem.ntnu.mobilewearprogramming.lab04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class LogIn extends AppCompatActivity {

    private static final int MAX_LENGTH = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        // Give a random username as example
        EditText username   = (EditText)findViewById(R.id.editText);
        username.setText(random());
    }


    // Gotten from https://stackoverflow.com/questions/12116092/android-random-string-generator
    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public void Submit(View view) {
        EditText username   = (EditText)findViewById(R.id.editText);
        Intent returnIntent = new Intent();
        returnIntent.putExtra("username",  username.getText().toString());
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
